var express = require('express');
var router = express.Router();
var DB = require('../service/base');

router.get('/', function(req, res, next) {
  var names = req.query.name.split(" ")
  var from = "("
  var where = ""
  var counter = 1
  for(nm of names) {
    from += "((LENGTH(dish.name) - LENGTH(REPLACE(lower(dish.name), '"+nm+"', '')))/LENGTH('"+nm+"'))"
    where += "lower(dish.name) like '%"+nm+"%'"
    if(counter < names.length) {
      from += " + "
      where += " or "
    }
    counter++
  }
  from += ") * 2 / ((LENGTH(dish.name) - LENGTH(REPLACE(dish.name, ' ', '')) + 1) + " + names.length + ")"
  DB.query("select "+from+" as relevent_score, dish.name as dish_name, restaurant.name as restaurant_name " +
      "from dish inner join restaurant on restaurant.id = dish.restaurant_id where "+where+" order by relevent_score desc, dish.name", null, function (data, error) {
    res.send(data);
  });
});

module.exports = router;

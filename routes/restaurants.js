var express = require('express');
var router = express.Router();
var DB = require('../service/base');

router.get('/hours', function(req, res, next) {
  DB.query("select distinct name from restaurant, operating_hour where restaurant.id = operating_hour.restaurant_id and operating_hour.date = '"+req.query.date+"'\n" +
      "and operating_hour.start_hour <= '"+req.query.time+"'\n" +
      "and '"+req.query.time+"' <= operating_hour.end_hour", null, function (data, error) {
    res.send(data);
  });
});

router.get('/prices', function(req, res, next) {
  DB.query("select count(distinct dish.id) as total_dishes, restaurant.name from restaurant, dish where restaurant.id = dish.restaurant_id and " +
  "dish.price >= "+req.query.price_from+" and dish.price <= "+req.query.price_to+" group by restaurant.name having total_dishes > "+req.query.dish_threshold+" order by restaurant.name limit 0," + req.query.top_number, null, function (data, error) {
    res.send(data);
  });
});

router.get('/', function(req, res, next) {
  var names = req.query.name.split(" ")
  var from = "("
  var where = ""
  var counter = 1
  for(nm of names) {
    from += "((LENGTH(restaurant.name) - LENGTH(REPLACE(lower(restaurant.name), '"+nm+"', '')))/LENGTH('"+nm+"'))"
    where += "lower(restaurant.name) like '%"+nm+"%'"
    if(counter < names.length) {
      from += " + "
      where += " or "
    }
    counter++
  }
  from += ") * 2 / ((LENGTH(restaurant.name) - LENGTH(REPLACE(restaurant.name, ' ', '')) + 1) + " + names.length + ")"
  DB.query("select "+from+" as relevent_score, name " +
  "from restaurant where "+where+" order by relevent_score desc, name", null, function (data, error) {
    res.send(data);
  });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var DB = require('../service/base');
var ETL = require('../etl');

router.post('/', function(req, res, next) {
  DB.query("select dish.price from dish inner join restaurant on dish.restaurant_id = restaurant.id where restaurant.name ='" + req.body.restaurant_name + "' and dish.name ='" + req.body.dish_name + "'", null, function (data, error) {
    var total_amount = data[0].price * req.body.quantity
    var queries = []
    queries.push("select * from user where id =" + req.body.user_id + " for update")
    queries.push("select * from restaurant where name ='" + req.body.restaurant_name + "' for update")
    queries.push("update restaurant set balance = balance + " + total_amount + " where name ='" + req.body.restaurant_name + "'")
    queries.push("update user set balance = balance - " + total_amount + " where id =" +req.body.user_id)
    queries.push("insert into transaction (id, restaurant_name, dish_name, quantity, total_amount) values ('"+generateUUID()+"','"+req.body.restaurant_name+"','"+req.body.dish_name+"',"+req.body.quantity+","+total_amount+")")
    let counter = 1
    DB.transaction(queries, null, function (status, error) {
      if(status && counter == queries.length + 1) {
        res.status(201)
        res.send('{"status": "success", "message": ""}');
      } else if(!status){
        try {
          res.status(400)
          res.send('{"status": "failed", "message": "' + error + '"}');
        } catch (e) {
          // do nothing
        }
      }
      counter++
    })
  });
});

function generateUUID() { // Public Domain/MIT
  var d = new Date().getTime();//Timestamp
  var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16;//random number between 0 and 16
    if(d > 0){//Use timestamp until depleted
      r = (d + r)%16 | 0;
      d = Math.floor(d/16);
    } else {//Use microseconds since page-load if supported
      r = (d2 + r)%16 | 0;
      d2 = Math.floor(d2/16);
    }
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

module.exports = router;

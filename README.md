How to Setup
- Clone the repo, put in directory name food_delivery
- Install MySQL
- Create database food_delivery
- Execute food_deliver/DDL.sql in the database that has just created
- Install NodeJS version 16.14.2
- Execute command:
    ````npm install  ````
- Update config.js for the MySQL configuration
- Run the ETL with this command in base directory of the project:

**for restaurant:**
    ````node etl.js restaurant https://gist.githubusercontent.com/seahyc/b9ebbe264f8633a1bf167cc6a90d4b57/raw/021d2e0d2c56217bad524119d1c31419b2938505/restaurant_with_menu.json````
    
**for user:**    
    ````node etl.js user https://gist.githubusercontent.com/seahyc/de33162db680c3d595e955752178d57d/raw/785007bc91c543f847b87d705499e86e16961379/users_with_purchase_history.json````
    
- Run the application: 
    ``node bin/www``

- The default host and port used is localhost:3000. There are 5 endpoints to accommodate the needs of this task:
    - **Search for restaurants by name, ranked by relevance to search term**
    
        path: /restaurants
        
        method: GET
        
        query parameter:
        - name - the name that we want to search in the restaurant
        
        response (200): 
        ``[
            {
              "relevent_score": 0.5,
              "name": "Eatmore Fried Chicken"
            },
            {
              "relevent_score": 0.5,
              "name": "Mad for Chicken"
            },
            {
              "relevent_score": 0.5,
              "name": "Oasis Fried Chicken"
            }
          ]``
    - **List top y restaurants that have more or less than x number of dishes within a price range, ranked alphabetically**

        path: /restaurants/prices
    
        method: GET
    
        query parameter:
        - top_number - the top y restaurants that we want to get
        - price_from - start price
        - price_to - end price
        - dish_threshold - minimum number of dishes in the price range
        
        response (200): 
        ``[
            {
              "total_dishes": 11,
              "name": "Antonia's"
            },
            {
              "total_dishes": 11,
              "name": "Azitra - Broomfield"
            },
            {
              "total_dishes": 11,
              "name": "Azurea at One Ocean"
            }
          ]``
    - **List all restaurants that are open at a certain datetime**

        path: /restaurants/times
    
        method: GET
    
        query parameter:
        - date - the date we want to search where the restaurant open: Mon, Tues, Weds, Thurs, Fri, Sat, Sun
        - time - the time we want to search where the restaurant open: in 24 hours format with minutes, ex: 1130, 1450
        
        response (200):
        ``[
            {
              "name": "Ellyngton's at the Brown Palace"
            },
            {
              "name": "Santiago's Bodega"
            },
            {
              "name": "Provenance"
            }
          ]``
    - **Search for dishes by name, ranked by relevance to search term**
    
        path: /dishes
        
        method: GET
        
        query parameter:
        - name - the name that we want to search in the dishes
        
        response (200):
        ``[
           {
              "relevent_score": 0.66666667,
              "dish_name": "1/4 chicken",
              "restaurant_name": "Gumbos"
            },
            {
              "relevent_score": 0.66666667,
              "dish_name": "Armenville (chicken",
              "restaurant_name": "Corridor"
            },
            {
              "relevent_score": 0.66666667,
              "dish_name": "Boiled Chicken",
              "restaurant_name": "Boulder Creek - Brownsburg"
            }
           ]``
    - **Process a user purchasing a dish from a restaurant**
    
        path: /purchase
        
        method: POST
        
        body request:
        ``{
              "quantity": 4,
              "dish_name": "1/4 chicken",
              "restaurant_name": "Gumbos",
              "user_id": 2
           }``
        
        response (201):
        ``{
              "status": "success",
              "message": ""
           }``
           
        response (400):
        ``{
              "status": "failed",
              "message": "Error: ER_SIGNAL_EXCEPTION: balance insufficient"
          }``
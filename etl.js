var args = process.argv
var DB = require('./service/base');

const https = require('https');
require('events').EventEmitter.prototype._maxListeners = 10000;

var mapDate = {
    'Mon': 0,
    'Tues': 1,
    'Weds': 2,
    'Thurs': 3,
    'Fri': 4,
    'Sat': 5,
    'Sun': 6
}
var revertMap = {
    0: 'Mon',
    1: 'Tues',
    2: 'Weds',
    3: 'Thurs',
    4: 'Fri',
    5: 'Sat',
    6: 'Sun'
}

if(args[2] == 'restaurant') {
    processRestaurant(args[3])
} else {
    processUser(args[3])
}

function processRestaurant(data) {
    https.get(data, res => {
        let body = '';
    console.log('Status Code:', res.statusCode);

    res.on('data', chunk => {
        body += chunk
    });

    res.on('end', () => {
        const restaurants = JSON.parse(body);
        let counter = 1
        let totalQuery = 0
        for(restaurant of restaurants) {
            var queries = generateOperatingHoursSQL(restaurant.openingHours, uuidRestaurant)
            totalQuery += restaurant.menu.length + 1 + queries.length
            var uuidRestaurant = generateUUID()
            DB.query("insert into restaurant (id, name, balance) values ('"+uuidRestaurant+"','"+restaurant.restaurantName.replaceAll("\\","\\\\").replaceAll("'","\\'")+"',"+restaurant.cashBalance+")", null, function (data, error) {
                if(counter == totalQuery){
                    console.log('DONE')
                    process.exit()
                }
                counter++
            });
            for (dish of restaurant.menu) {
                var uuidDish = generateUUID()
                DB.query("insert into dish (id, name, price, restaurant_id) values ('"+uuidDish+"','"+dish.dishName.replaceAll("\\","\\\\").replaceAll("'","\\'")+"',"+dish.price+",'"+uuidRestaurant+"')", null, function (data, error) {
                    if(counter == totalQuery){
                        console.log('DONE')
                        process.exit()
                    }
                    counter++
                });
            }
            for (query of queries) {
                var uuidOperatingHour = generateUUID()
                DB.query(query, null, function (data, error) {
                    if(counter == totalQuery){
                        console.log('DONE')
                        process.exit()
                    }
                    counter++
                });
            }
        }
});
})
}

function processUser(data) {
    https.get(data, res => {
        let body = '';
    console.log('Status Code:', res.statusCode);

    res.on('data', chunk => {
        body += chunk
    });

    res.on('end', () => {
        const users = JSON.parse(body);
        let counter = 1
        let totalQuery = 0
        for(user of users) {
            totalQuery += 1 + user.purchaseHistory.length
            DB.query("insert into user (id, name, balance) values ("+user.id+",'"+user.name.replaceAll("\\","\\\\").replaceAll("'","\\'")+"',"+user.cashBalance+")", null, function (data, error) {
                if(counter == totalQuery){
                    console.log('DONE')
                    process.exit()
                }
                counter++
            });
            for (transaction of user.purchaseHistory) {
                var uuidTransaction = generateUUID()
                var transactionDate = new Date(Date.parse(transaction.transactionDate));
                DB.query("insert into transaction (id, restaurant_name, dish_name, total_amount, transaction_date) values ('"+uuidTransaction+"','"+transaction.restaurantName.replaceAll("'","\\'")+"','"+transaction.dishName.replaceAll("\\","\\\\").replaceAll("'","\\'")+"',"+transaction.transactionAmount+",'"+transactionDate.getFullYear()+"-"+("0" + (transactionDate.getMonth() + 1)).slice(-2)+"-"+("0" + transactionDate.getDate()).slice(-2)+" "+("0" + transactionDate.getHours()).slice(-2)+":"+("0" + transactionDate.getMinutes()).slice(-2)+":"+("0" + transactionDate.getSeconds()).slice(-2)+"')", null, function (data, error) {
                    if(counter == totalQuery){
                        console.log('DONE')
                        process.exit()
                    }
                    counter++
                });
            }
        }
    });
}).on('error', err => {
        console.log('Error: ', err.message);
});
}

function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

function generateOperatingHoursSQL(data, restaurant_id) {
    var result = []
    var schedules = data.split("/")
    for(schedule of schedules) {
        //get hour
        const reHour = /([a-zA-Z ,-]+)([0-9]+)[: ]*([0-9]*) (am|pm)[- ]+([0-9]+)[: ]*([0-9]*) (am|pm)/g;
        const hours = reHour.exec(schedule);
        var startHours = []
        var endHours = []
        var startHour = ''
        var endhour = ''
        var startMinute = ''
        var endMinute = ''
        if(hours[4] == 'pm') {
            hours[2] = ((parseInt(hours[2]) + 12) % 24).toString()
        }
        if(hours[7] == 'pm') {
            hours[5] = ((parseInt(hours[5]) + 12) % 24).toString()
        }
        while(hours[2].length < 2) {
            hours[2] = '0' + hours[2]
        }
        startHour = hours[2]
        while(hours[5].length < 2) {
            hours[5] = '0' + hours[5]
        }
        endhour = hours[5]
        if(hours[3] == undefined) {
            hours[3] = '00'
        }
        while(hours[3].length < 2) {
            hours[3] = '0' + hours[3]
        }
        startMinute = hours[3]
        if(hours[6] == undefined) {
            hours[6] = '00'
        }
        while(hours[6].length < 2) {
            hours[6] = '0' + hours[6]
        }
        endMinute = hours[6]
        if(startHour > endhour || (startHour == endhour && startMinute > endMinute)) {
            startHours.push(startHour + startMinute)
            endHours.push('2359')
            startHours.push('0000')
            endHours.push(endhour+endMinute)
        } else {
            startHours.push(startHour + startMinute)
            endHours.push(endhour+endMinute)
        }
        // get date
        var dates = hours[1].split(",")
        for(date of dates){
            const reDate = /([a-zA-Z]+)[ -]*([a-zA-Z]*)/g;
            const dateResult = reDate.exec(date.trim());
            if(dateResult[2] != '') {
                for (i = mapDate[dateResult[1]]; i <= mapDate[dateResult[2]]; i++) {
                    var index = 0
                    for (hour of startHours) {
                        var uuidOperatingHour = generateUUID()
                        result.push("insert into operating_hour (id, restaurant_id, start_hour, end_hour, date) values ('" + uuidOperatingHour + "','" + restaurant_id + "','" + hour + "','" + endHours[index] + "','" + revertMap[(i+index) % 7] + "')")
                        index++
                    }
                }
            } else {
                for (i = 0; i < 7; i++) {
                    if(dateResult != null && dateResult[1].includes(revertMap[i])) {
                        var index = 0
                        for (hour of startHours) {
                            var uuidOperatingHour = generateUUID()
                            result.push("insert into operating_hour (id, restaurant_id, start_hour, end_hour, date) values ('" + uuidOperatingHour + "','" + restaurant_id + "','" + hour + "','" + endHours[index] + "','" + revertMap[(i+index) % 7] + "')")
                            index++
                        }
                    }
                }
            }
        }
    }
    return result
}
var mysql   = require("mysql");
var env = process.env.NODE_ENV || 'development';
var config = require('../config')[env];
var success = true
var pool = mysql.createPool({
    connectionLimit : 100,
    host: config.database.host,
    user: config.database.username,
    password: config.database.password,
    database: config.database.db
});

var DB = (function () {
    function _query(query, params, callback) {
        pool.getConnection(function (err, connection) {
            if (err) {
                try{
                    connection.release();
                } catch (e) {
                    // do nothing
                }
                callback(null, err);
            }

            connection.query(query, params, function (err, rows) {
                try{
                    connection.release();
                } catch (e) {
                    // do nothing
                }
                if (!err) {
                    callback(rows);
                }
                else {
                    callback(null, err);
                }
            });

            connection.on('error', function (err) {
                try{
                    connection.release();
                } catch (e) {
                    // do nothing
                }
                callback(null, err);
            });
        });
    };

    function _transaction(queries, params, callback) {
        pool.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                if (err) {                  //Transaction Error (Rollback and release connection)
                    connection.rollback(function() {
                        try {
                            connection.release();
                            success = false
                        } catch (e) {
                            success = false
                        }
                        callback(false, err)
                    });
                } else {
                    for(query of queries) {
                        connection.query(query, params, function(err, results) {
                            if (err) {          //Query Error (Rollback and release connection)
                                connection.rollback(function() {
                                    success = false
                                    try{
                                        connection.release();
                                    } catch (e) {
                                        // do nothing
                                    }
                                    callback(false, err)
                                });
                            } else {
                                callback(true, err)
                            }
                        });
                    }

                    connection.commit(function(err) {
                        if (err) {
                            connection.rollback(function() {
                                try{
                                    connection.release();
                                } catch (e) {
                                    // do nothing
                                }
                                callback(false, err)
                            });
                        } else {
                            try{
                                connection.release();
                            } catch (e) {
                                // do nothing
                            }
                            callback(true, null)
                        }
                    });
                }
            });
        });
    };

    return {
        query: _query,
        transaction: _transaction
    };
})();

module.exports = DB;
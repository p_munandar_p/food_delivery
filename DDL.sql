create table dish
(
	id varchar(36) not null
		primary key,
	name mediumtext not null,
	price float not null,
	restaurant_id varchar(36) not null,
	created_date timestamp default CURRENT_TIMESTAMP not null
);

create table operating_hour
(
	id varchar(36) not null
		primary key,
	restaurant_id varchar(36) not null,
	start_hour varchar(10) not null,
	date varchar(10) not null,
	end_hour varchar(10) not null
);

create index operating_hour_start_hour_end_hour_date_index
	on operating_hour (start_hour, end_hour, date);

create table restaurant
(
	id varchar(36) not null
		primary key,
	name mediumtext not null,
	balance float default 0 not null,
	created_date timestamp default CURRENT_TIMESTAMP not null
);

create table transaction
(
	id varchar(36) not null
		primary key,
	restaurant_name varchar(255) not null,
	dish_name varchar(255) not null,
	quantity int default 1 not null,
	total_amount float not null,
	transaction_date timestamp default CURRENT_TIMESTAMP not null
);



create table user
(
	id bigint not null
		primary key,
	name mediumtext not null,
	balance float default 0 not null,
	created_date timestamp default CURRENT_TIMESTAMP not null
);

CREATE TRIGGER balanceCheck
  BEFORE UPDATE
  ON user
  FOR EACH ROW
BEGIN
  IF NEW.balance <= 0 THEN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = 'balance insufficient';
  END IF;
END;